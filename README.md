# AngularSchoolC6

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.0.

This course is made of an angular app, that will evolve along the chapters, and a FAKE data server, made with [JSON server](https://github.com/typicode/json-server).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Fake data server

Run `node server/server.js` to launch the fake data server in `http://localhost:3000`.
The data is served and stored in: `server/db.json`.

The endpoints available are:
* `contacts`
* `contacts/{id}`
* `users`
* `users/{id}`

Any request should be made setting a bearer token authentication in the request headers, matching the auth token of some of the users in database (for example `Authorization: Bearer 1234567890`).

# CodelabAngularDevops

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.1.

# 0

```
ng new codelab-angular-devops --enable-ivy
```


# 1

```
customLaunchers: {
  ChromeHeadlessCI: {
    base: 'ChromeHeadless',
    flags: ['--no-sandbox']
  }
},
```


# 2

```
"test:ci": "ng test --no-watch --code-coverage --browsers=ChromeHeadlessCI",
```

# 3

```
image: node:10.16.0

stages:
  - install
  - test
  - build
  - deploy
```

# 4

```
install:
  stage: install
  script: 
    - npm install
  artifacts:
    expire_in: 1h
    paths:
      - node_modules/
  cache:
    paths:
      - node_modules/
```

# 5

```
tests:
  stage: test
  variables:
    CHROME_BIN: google-chrome
  dependencies:
    - install
  before_script:
    - apt-get update && apt-get install -y apt-transport-https
    - wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
    - sh -c 'echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
    - apt-get update && apt-get install -y google-chrome-stable
  script:
    - npm run test:ci
  coverage: '/Statements.*?(\d+(?:\.\d+)?)%/'
```

# 6

```
build:
  stage: build
  variables:
    BUILD_CONFIG: 'production'
  dependencies:
    - install
  script:
    - npm run build:ci
  artifacts:
    expire_in: 1h
    paths:
      - dist/
```

# 7

```
"build:ci": "ng build -c=$BUILD_CONFIG --baseHref=/codelab-angular-devops/",
```

# 8

```
build:
  stage: build
  variables:
    BUILD_CONFIG: 'production'
  dependencies:
    - install
  script:
    - npm run build:ci
  artifacts:
    expire_in: 1h
    paths:
      - dist/
  only:
    - master
```


# 9

```
pages:
  stage: deploy
  dependencies:
    - build
  script:
    - mkdir public
    - mv ./dist/codelab-angular-devops/* ./public/
    - cp ./public/index.html public/404.html
  artifacts:
    paths:
      - public/
  environment:
    name: production
  only:
    - master
```



